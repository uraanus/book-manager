/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.bookmanager.service.serviceImpl;

import com.library.bookmanager.entity.Book;
import com.library.bookmanager.service.BookService;
import com.library.bookmanager.utility.FileOperations;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author elshemy
 */
public class BookServiceImpl implements BookService {

    FileOperations fo = new FileOperations();

    @Override
    public int insertBook(Book b) {
        List<Book> allbooks = getAllBooks();
        Book lastBook = allbooks.get(allbooks.size() - 1);
        int bookId = lastBook.getId() + 1;
        b.setId(bookId);
        String bookAsAText = "";
        if (allbooks.size() > 0) {
            bookAsAText += "\n";
        }
        bookAsAText += b.getId() + "|" + b.getTitle() + "|" + b.getAuthor() + "|" + b.getDescription();
        fo.writeToFile(bookAsAText, true);
        return bookId;
    }

    @Override
    public void editBook(Book b) {
        List<Book> allBooks = getAllBooks();
        String bookAsAText = "";
        int listCounter = 0;
        for (Book book : allBooks) {
            listCounter++;
            if (b.getId() == book.getId()) {
                if (b.getAuthor().length() > 0) {
                    book.setAuthor(b.getAuthor());
                }
                if (b.getTitle().length() > 0) {
                    book.setTitle(b.getTitle());
                }
                if (b.getDescription().length() > 0) {
                    book.setDescription(b.getDescription());
                }
            }
            bookAsAText += book.getId() + "|" + book.getTitle() + "|" + book.getAuthor() + "|" + book.getDescription();
            if (listCounter != allBooks.size()) {
                bookAsAText += "\n";
            }
        }
        fo.writeToFile(bookAsAText, false);
    }

    @Override
    public List<Book> getBookByTitle(String keyword) {
        List<Book> allBooks = getAllBooks();
        List<Book> matcchedBooks = new ArrayList<>();
        for (Book b : allBooks) {
            if (b.getTitle().contains(keyword)) {
                matcchedBooks.add(b);
            }
        }
        return matcchedBooks;
    }

    @Override
    public List<Book> getAllBooks() {
        List<String> allLines = fo.readAllFile();
        List<Book> books = new ArrayList<>();
        for (String l : allLines) {
            String bookAsArray[] = l.split("\\|");
            Book b = new Book(Integer.parseInt(bookAsArray[0]), bookAsArray[1], bookAsArray[2], bookAsArray[3]);
            books.add(b);
        }
        return books;
    }

    @Override
    public Book getBookById(int id) {
        List<Book> books = getAllBooks();
        for (Book b : books) {
            if (b.getId() == id) {
                return b;
            }
        }
        return null;
    }
}
