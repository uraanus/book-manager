/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.library.bookmanager.service;

import com.library.bookmanager.entity.Book;
import java.util.List;

/**
 *
 * @author elshemy
 */
public interface BookService {
    public int insertBook(Book b);
    public void editBook(Book b);
    public List<Book> getBookByTitle(String keyword);
    public List<Book> getAllBooks();
    public Book getBookById(int id);
    
}
