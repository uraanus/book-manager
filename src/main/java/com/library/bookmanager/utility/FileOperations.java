/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.bookmanager.utility;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author elshemy
 */
public class FileOperations {

    public void writeToFile(String data, boolean append) {
        File file = new File("src/main/resources/books.txt");
        FileWriter fr;
        try {
            fr = new FileWriter(file, append);
            fr.write(data);
            fr.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public List<String> readAllFile() {
        List<String> allLines = null;
        try {
            allLines = Files.readAllLines(Paths.get("src/main/resources/books.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allLines;
    }
}
