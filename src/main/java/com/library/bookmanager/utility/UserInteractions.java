/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.bookmanager.utility;

import com.library.bookmanager.entity.Book;
import com.library.bookmanager.service.BookService;
import com.library.bookmanager.service.serviceImpl.BookServiceImpl;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author elshemy
 */
public class UserInteractions {

    BookService bs = new BookServiceImpl();

    public void displayChoices() {
        String choices = "===== Book Manager ===== \n";
        choices += "\t 1) View all Books \n";
        choices += "\t 2) Add Book \n";
        choices += "\t 3) Edit Book \n";
        choices += "\t 4) Search for Book \n";
        choices += "\t 5) Save And Exit \n";
        System.out.println(choices);
    }

    public void startApp() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            displayChoices();
            int input = sc.nextInt();
            if (input == 1) {
                displayBooksPressed();
            } else if (input == 2) {
                addBookPressed();
            } else if (input == 3) {
                editBookPressed();
            } else if (input == 4) {
                searchForBookPressed();
            } else if (input == 5) {
                System.out.println("Library Saved \n Good Bye");
                break;
            } else {
                System.out.println("Please Choose one of the choices above!");
            }
        }
    }

    public void displayBooksPressed() {
        Scanner sc = new Scanner(System.in);
        displayBooks(bs.getAllBooks());
        System.out.println("To view details enter the book ID, to return press <Enter>.");
        while (true) {

            String addInput = sc.nextLine();
            if (addInput.length() == 0) {
                break;
            } else {
                displayDetailedBook(Integer.parseInt(addInput));
            }
        }
    }

    public void displayBooks(List<Book> books) {
        String booksAsString = "";
        for (Book b : books) {
            booksAsString += "[" + b.getId() + "] " + b.getTitle() + "\n";
        }
        System.out.println(booksAsString);
    }

    public void displayDetailedBook(int id) {

        Book b = bs.getBookById(id);
        String detailedBook = "Book ID :" + b.getId() + "\n";
        detailedBook += "Title : " + b.getTitle() + "\n";
        detailedBook += "Author : " + b.getAuthor() + "\n";
        detailedBook += "Descripption : " + b.getDescription() + "\n";
        System.out.println(detailedBook);
        System.out.println("To view details enter the book ID, to return press <Enter>.");
        System.out.println("Book Id : ");
    }

    public void addBookPressed() {
        Scanner sc = new Scanner(System.in);
        System.out.println("insert title :");
        String title = sc.nextLine();

        System.out.println("insert author :");
        String author = sc.nextLine();

        System.out.println("insert description :");
        String description = sc.nextLine();

        Book b = new Book();
        b.setTitle(title);
        b.setAuthor(author);
        b.setDescription(description);
        BookService bs = new BookServiceImpl();
        int bookId = bs.insertBook(b);
        System.out.println("Book[" + bookId + "] Saved");
    }

    public void editBookPressed() {
        System.out.println("======= Edit Book ======");
        displayBooks(bs.getAllBooks());
        while (true) {
            System.out.println("Enter the book ID of the book you want to edit,to return press <Enter>.");
            Scanner sc = new Scanner(System.in);
            String addInput = sc.nextLine();
            if (addInput.length() == 0) {
                break;
            } else {
                editBook(Integer.parseInt(addInput));
            }
        }
    }

    public void editBook(int bookId) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input the following information. To leave a field unchanged, hit <Enter>");
        Book editedBook = bs.getBookById(bookId);
        System.out.println("Title [" + editedBook.getTitle() + "] : ");
        String titleInput = sc.nextLine();
        if (titleInput.length() > 0) {
            editedBook.setTitle(titleInput);
        }

        System.out.println("Author [" + editedBook.getAuthor() + "] : ");
        String authorInput = sc.nextLine();
        if (authorInput.length() > 0) {
            editedBook.setAuthor(authorInput);
        }

        System.out.println("Description [" + editedBook.getDescription() + "] : ");
        String descInput = sc.nextLine();
        if (descInput.length() > 0) {
            editedBook.setDescription(descInput);
        }

        bs.editBook(editedBook);
        System.out.println("Book Saved");
    }

    public void searchForBookPressed() {
        System.out.println("===== Search ========");
        System.out.println("Type a keyword of title to search in.");
        Scanner sc = new Scanner(System.in);
        String keyword = sc.nextLine();
        List<Book> matchedBooks = bs.getBookByTitle(keyword);
        System.out.println("The following books matched your query. Enter the book ID to see more details, or <Enter> to return");
        displayBooks(matchedBooks);
        System.out.println("To view details enter the book ID, to return press <Enter>.");
        while (true) {
            String addInput = sc.nextLine();
            if (addInput.length() == 0) {
                break;
            } else {
                displayDetailedBook(Integer.parseInt(addInput));
            }
        }

    }
}
